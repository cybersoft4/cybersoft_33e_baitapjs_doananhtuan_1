// B1: tính tiền lương nhân viên
// Input: lương 1 ngày, số ngày làm
// Action: 
// s1: khai báo 2 biến để nhận input, 1 biến để nhận output
// s2: thực hiện phép tính để tính kết quả:
// (lương 1 ngày*số ngày làm)
// s3: in ra kết quả
// Output: tiền lương nhân viên

const salaryOneDay=100000
var numberWorkDay
numberWorkDay=4
var salary=salaryOneDay*numberWorkDay
console.log(salary)

// B2: tính giá trị trung bình
// Input: giá trị 5 số thực
// Action: 
// s1: khai báo 5 biến để nhận input, 1 biến để nhận output
// s2: thực hiện phép tính để tính kết quả 
// (tổng 5 số)/5
// s3: in ra kết quả
// Output: giá trị trung bình

let a=1
let b=2
let c=3
let d=4
let e=5
let averageValue=(a+b+c+d+e)/5
console.log(averageValue)

// B3: quy đổi tiền
// Input: số tiền USD, giá USD hiện nay
// Action: 
// s1: khai báo 2 biến để nhận input, 1 biến để nhận output
// s2: thực hiện phép tính để tính kết quả 
// (số tiền USD * giá USD hiện nay)
// s3: in ra kết quả
// Output: số tiền VND

const exchangeRate=23500
let moneyUSD=3
let moneyVND=exchangeRate*moneyUSD
console.log(moneyVND)

// B4: Tính chu vi, diện tích hình chữ nhật
// Input: chiều rộng, chiều dài
// Action: 
// s1: khai báo 2 biến để nhận input, 2 biến để nhận output
// s2: thực hiện phép tính để tính kết quả 
// (diện tích= chiều rộng * chiều dài; chu vi=(chiều rộng+ chiều dài)/2)
// s3: in ra kết quả
// Output: chu vi, diện tích

const width=30;
const height=50;
const chuVi=(width+height)/2
const dienTich=(width*height)
console.log(chuVi,dienTich)

// B5: Tính tổng 2 ký số
// Input: 1 số có 2 chữ số (ab)
// Action: 
// s1: khai báo 1 biến để nhận input,2 biến để nhận giá trị đầu và cuối, 1 biến để nhận output
// s2: thực hiện phép tính để tính kết quả 
// (a=ab%10; b=Match.floor(ab/10)==>c=a+b)
// s3: in ra kết quả
// Output: tổng 2 ký số

const ab=12
const donVi = ab%10
const hangChuc= Math.floor(ab/10)

const tongKySo=donVi+hangChuc
console.log(tongKySo)



